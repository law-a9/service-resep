from .models import Order, OrderObat
from django.contrib import admin

admin.site.register(Order)
admin.site.register(OrderObat)
