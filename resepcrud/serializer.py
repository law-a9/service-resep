from .models import OrderObat, Order
from rest_framework import serializers


class ObatOrderSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = OrderObat
        fields = ['jumlah', 'nama', 'harga_satuan', 'dosis']


class ObatListSerializer(serializers.ModelSerializer):
    order = ObatOrderSerializer(many=True)
    
    class Meta:
        model = Order
        fields = ['order', 'uid']

    def create(self, validated_data):
        orders = validated_data['order']
        uid = validated_data['uid']
        order_id = self.context['order_id']
        order_object = Order.objects.create(uid=uid, order_id=order_id)
        created_order = []

        for order_data in orders:
            order_data['order_id'] = order_object
            created_order.append(OrderObat.objects.create(**order_data))
        
        return {'order':created_order, 'uid':uid}
