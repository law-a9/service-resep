from .views import ObatOrderView
from django.urls import path

app_name = 'resepcrud'

urlpatterns = [
    path("<int:order_id>", ObatOrderView.as_view(), name="list_obat"),
]
