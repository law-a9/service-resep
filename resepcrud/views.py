from .serializer import ObatListSerializer 
from .models import Order

from rest_framework.generics import RetrieveUpdateDestroyAPIView, CreateAPIView


class ObatOrderView(CreateAPIView, RetrieveUpdateDestroyAPIView):
    serializer_class = ObatListSerializer
    lookup_url_kwarg = 'order_id'
    lookup_field = 'order_id'

    def get_serializer_context(self):
        return {
            'request': self.request,
            'format': self.format_kwarg,
            'view': self,
            'order_id': self.kwargs.get(self.lookup_url_kwarg)
        }
    def perform_create(self, serializer):
        serializer.save()

    def get_queryset(self):
        order_id = self.kwargs.get(self.lookup_url_kwarg)
        return Order.objects.filter(order_id=order_id)