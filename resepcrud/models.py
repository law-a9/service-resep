from django.db import models


class Order(models.Model):
    order_id = models.IntegerField(unique=True)
    uid = models.CharField(max_length=30)

    def __str__(self) -> str:
        return self.uid + " (" + str(self.order_id) + ")"


class OrderObat(models.Model):
    jumlah = models.IntegerField()
    nama = models.CharField(max_length=30)
    harga_satuan = models.IntegerField()
    dosis = models.CharField(max_length=30)
    order_id = models.ForeignKey(Order, on_delete=models.CASCADE, related_name="order")

    def __str__(self) -> str:
        return self.nama
